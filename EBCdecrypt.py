

from Crypto.Cipher import AES
import base64
def AESserver(text):
	secret = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
	secret = secret.decode("base64")
	pt = text + secret
	x = 16-(len(pt)%16)
	pt = pt + x*chr(x)
	obj=AES.new("aaaabbbbccccdddd",AES.MODE_ECB)
	ct = obj.encrypt(pt)
	return ct


if __name__ == "__main__":	
	x = "a" * 15 	
	x = AESserver(x)
	l = len(x)/16	
	flag = ""
	for i  in range(1,l):
		for j in range(15,-1,-1):
			text = "a" * j
			cipher = AESserver(text)
			for k in range(256):
				trial = text + flag + chr(k) 
				ciphertrial = AESserver(trial)	
				if (ciphertrial[:16*i] == cipher[:16*i]):
					flag += chr(k)
					break
	print flag		
		
